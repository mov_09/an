$(function(){
	$('.js-checkbox-label').on('click', function(e) {
		e.stopPropagation();
		e.preventDefault();

		var input = $(this).parent().find('input');
		if (input.prop('checked')) {
			input.prop('checked', false);
		} else {
			input.prop('checked', true);
		}

		checkActiveRows($('.an_table .js-checkbox'), $('.js-export'));
	});

	$('.js-edit').on('click', function(e){
		e.stopPropagation();
		e.preventDefault();

		window.location.href = $(this).attr('href');
	});

	$('.js-export').on('click', function(){
		if ($(this).hasClass('active')) {
			$('.modal-export').modal('show');
		}
	});
	checkActiveRows($('.an_table .js-checkbox'), $('.js-export'));

	$('.modal-export').on('shown.bs.modal', function (e) {
		checkActiveRows($('.modal-export .js-checkbox'), $('.modal-export .btn'));
	});
	$('.modal-export .js-checkbox').on('change', function(){
		checkActiveRows($('.modal-export .js-checkbox'), $('.modal-export .btn'));
	});

	// preven open few dropdowns
	$('.js-dropdown').on('click', function(e) {
		// console.log($(e.target));
		// console.log($(e.target).hasClass('.btn-filter'));
		if ($(document).width() < 1366 && $(e.target).hasClass('btn-filter')) {
			console.log('+');
			e.preventDefault();
			e.stopPropagation();
			$('.js-dropdown').removeClass('open');
			$(this).addClass('open');
		}
	});

	// on small screen checkboxes work like select
	$('.js-dropdown-select').on('click', 'label', function(e){
		if ($(document).width() < 1366) {
			var dropdown = $(this).closest('.dropdown'),
				btn = dropdown.find('.btn-filter');

			btn.text($(this).text());
			dropdown.removeClass('open');
		} else {
			return;
		}
	});
});
function checkActiveRows(checkboxes, button_selector) {
	if (_.some(checkboxes, ['checked', true])) {
		toggleActiveBtn(button_selector, 'add');
	} else {
		toggleActiveBtn(button_selector, 'remove');
	}
}
function toggleActiveBtn(button_selector, action) {
	eval('button_selector.' + action + 'Class("active")');
	if (button_selector.length > 1) {
		$.each(button_selector, function(i, el){
			if (action == 'add') {
				$(el).prop('disabled', false);
			} else {
				$(el).prop('disabled', true);
			}
		});
	}
}
