var slider;
$(function(){
	$('#rightbar').css('top', $('#page_header').innerHeight());

	$(document).on('click', '.news_btn', function(){
	$('#rightbar, #fill').toggleClass('visible');
	});

	$("#rightbar").customScrollbar();

	$('.modal').on('shown.bs.modal', function (e) {
		setTimeout(function(){
		initSlider();
		initShowTags();
		}, 500);
		if ($(".js-scroll").length) {
			$(".js-scroll").customScrollbar();
		}
	});
	$('.js-collapsed-desc').on('click', '.js-show-desc', function(){
		$(this).parent().find('.js-full-desc').toggleClass('hidden');
		$(this).toggleClass('open');
	});

	function addInput() {
		var inputId = 1;
		$('.js-add-field').on('click', function() {
			$(this).parent().after(
				'<div class="form-group">' +
					'<input class="form-control" name="personal_data[phone' + inputId + ']" id="personal_data[phone' + inputId + ']" type="text" />' +
				'</div>'
			);
			inputId++;
		});
	}

	addInput();

	$('.js-load-photo').on('click', function() {
		// $('body').find('.input-file').trigger('click');
		$(this).parent().find('.input-file').trigger('click');
	});
	$('.input-file').on('change', function(evt) {
		handleFileSelect(evt, $(this));
	});

	function dropdownInit() {
		$('.dropdown-button').on('click', function (event) {
			$(this).parent().toggleClass('open');
		});

		$('body').on('click', function (e) {
			if (!$('.dropdown').is(e.target)
				&& $('.dropdown').has(e.target).length === 0
				 && $('.open').has(e.target).length === 0
			) {
				$('.dropdown').removeClass('open');
			}
		});
	}

	dropdownInit();

	$('.custom-select select').selectpicker({
		size: 6,
	});

	// price placeholder switch

	$(".js-price-value").click(function(){
		$(".js-price-placeholder").attr("placeholder", $(this).text());
	})


	// input file filename

	$('.js-file').change(function() {
		var files = document.getElementById("file2").files;
		var filesname = "";
		for (var i = 0; i < files.length; i++) {
			filesname += files[i].name + "\n";
			$('.js-filename').text(filesname);
		}
	});

	// select rubric

	$(".js-select-rubric select").on("change", function(){
		var selectValue = $(this).val();
		$(this).parents(".box").before(
			'<div class="box form-elements">' +
				'<div class="box-content row">' +
					'<div class="col-md-4">' +
						'<div class="title">Выбор рубрики</div>' +
						'<div class="custom-select">' +
							'<select name="select" data-style="btn-default" title="Выбрать рубрику">' +
								'<option value="Купить квартиру" selected>' + selectValue + '</option>' +
							'</select>' +
						'</div>' +
					'</div>' +
					'<div class="col-md-7">' +
						'<div class="title">Район покупки</div>' +
						'<div class="dropdown select-area">' +
							'<button id="select-area" class="btn-default dropdown-button" type="button" aria-haspopup="true" aria-expanded="false">' +
								'Выбрать район' +
								'<span class="caret"></span>' +
							'</button>' +
							'<div class="dropdown-menu" aria-labelledby="select-area">' +
								'<div class="clear clearfix">' +
									'<span class="select-all">Выбрать все</span>' +
									'<span class="clear-button">Очистить все</span>' +
								'</div>' +
								'<div class="collapse-group">' +
									'<div class="col-lg-6">' +
										'<button class="collapse-button" type="button" data-toggle="collapse" data-target="#collapse" aria-expanded="false" aria-controls="collapse">' +
											'Одесса' +
											'<span class="caret"></span>' +
										'</button>' +
										'<div class="collapse collapse-content" id="collapse">' +
											'<ul>' +
												'<li>' +
													'<input id="check" class="an_chekbox" type="checkbox" name="check" checked>' +
													'<label for="check">Киевский</label>' +
												'</li>' +
											'</ul>' +
										'</div>' +
									'</div>' +
								'</div>' +
							'</div>' +
						'</div>' +
					'</div>' +
					'</div>' +
					'<div class="box-content row">' +
						'<div class="col-sm-11 col-md-6">' +
							'<div class="title">Пожелания</div>' +
							'<div class="form-group add-more">' +
								'<input class="form-control" type="text" />' +
								'<button class="btn btn-plus js-add-field" type="button"></button>' +
							'</div>' +
						'</div>' +
					'</div>' +
					'<div class="box-content row">' +
						'<div class="col-md-12">' +
							'<div class="title">Сумма покупки</div>' +
							'<div class="custom-radio">' +
								'<input id="radio1" type="radio" name="radio" checked>' +
								'<label for="radio1">Общая</label>' +
							'</div>' +
							'<div class="custom-radio">' +
								'<input id="radio2" type="radio" name="radio">' +
								'<label for="radio2">За м<sup>2</sup></label>' +
							'</div>' +
							'<div class="input-group form-control">' +
								'<input type="text" placeholder="От">' +
								'<span>-</span>' +
								'<input type="text" placeholder="До">' +
							'</div>' +
							'<div class="custom-radio">' +
								'<input id="radio3" type="radio" name="radio1" checked>' +
								'<label for="radio3">$</label>' +
							'</div>' +
							'<div class="custom-radio">' +
								'<input id="radio4" type="radio" name="radio1">' +
								'<label for="radio4">Грн</label>' +
							'</div>' +
						'</div>' +
					'</div>' +
				'</div>' +
			'</div>'
		);
		$(".custom-select select").selectpicker('refresh');
		dropdownInit();
		addInput();
		$(this).val('').selectpicker('refresh');
	})

	// fancybox

	$(".fancybox").fancybox({
		padding: 0,
	});

	// gallery modal

	$(".js-open-gallery").on("click", function() {

		$('#gallery').on('shown.bs.modal', function () {

			$('.gallery-main').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				dots: false,
				fade: true,
				asNavFor: '.gallery-thumb'
			});

			$('.gallery-thumb').slick({
				slidesToShow: 8,
				slidesToScroll: 1,
				asNavFor: '.gallery-main',
				dots: false,
				arrows: false,
				focusOnSelect: true
			});

		})

	})

});

function initShowTags() {
	$('.js-show-tags').on('click', function(){
		$(this).addClass('hidden');
		$(this).parent().parent().find('span').removeClass('hidden');
	});
}
var sliderInited = false,
	slider;
function initSlider() {
	if (!sliderInited && $(".slider").length) {
		slider = $(".slider");

		slider.on('init', function() {
			slider.removeClass('not-loaded');
		});

		slider = $(".slider").slick({
			// centerMode: true,
			slidesToShow: 4,
			customPaging: '15px',
			variableWidth: true,
			infinite: false,
			arrows: false,
			dots: false,
			initialSlide: 0
		});
	}
	sliderInited = true;
}

// show image load on input
function handleFileSelect(evt, el) {
	var files = evt.target.files; // FileList object

	// Loop through the FileList and render image files as thumbnails.
	for (var i = 0, f; f = files[i]; i++) {

		// Only process image files.
		if (!f.type.match('image.*')) {
			continue;
		}

		var reader = new FileReader();

		// Closure to capture the file information.
		reader.onload = (function(theFile) {
			return function(e) {
				// Render thumbnail.
				$(el).parent().find('.image').removeClass('empty');
				$(el).parent().find('.photo-preview').attr('src', e.target.result);
			};
		})(f);

		// Read in the image file as a data URL.
		reader.readAsDataURL(f);
	}
}
