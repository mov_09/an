var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    cleanCSS = require('gulp-clean-css'),
    concat = require('gulp-concat'),
    compress = require('gulp-yuicompressor'),
	connect = require('gulp-connect'),
	fileinclude = require('gulp-file-include');

var config = {
  stylesMain: './sourse/styles/app.scss',
  CSSSource:  './sourse/styles/',
  CSSDest:    './css',
  jsSource:   './sourse/js/',
  jsDest:     './js',
  bowerSrc:   './bower_components/',
  htmlSourse: 	'./sourse/html/',
  root:		  './'
};

gulp.task('sass-sourcemaps', function () {
  return gulp.src(config.stylesMain)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(config.CSSDest));
});

gulp.task('sass-no-sourcemaps', function () {
  return gulp.src(config.stylesMain)
    .pipe(sass().on('error', sass.logError))
    .pipe(cleanCSS())
    .pipe(gulp.dest(config.CSSDest));
});

var copyJS = [
  config.bowerSrc + 'jquery/dist/jquery.min.js',
  config.jsSource + 'bootstrap.min.js',
  config.bowerSrc + 'slick-carousel/slick/slick.js',
  config.bowerSrc + 'bootstrap-select/js/bootstrap-select.js',
  config.bowerSrc + 'lodash/dist/lodash.js',
  config.jsSource + 'custom-scrollbar.min.js',
  config.jsSource + 'jquery.fancybox.pack.js',
  config.jsSource + 'jquery.fancybox-thumbs.js',
  config.jsSource + 'general.js',
  config.jsSource + 'tables.js'
];

// concat js files
gulp.task('js', function(){
  gulp.src(copyJS)
      .pipe(compress({type: 'js'}))
      .pipe(concat('app.min.js'))
      .pipe(gulp.dest(config.jsDest));
});

// html partials
gulp.task('fileinclude', function() {
  gulp.src([config.htmlSourse + '*.html', config.htmlSourse + 'partials/*.html'])
    .pipe(fileinclude({}))
    .pipe(gulp.dest(config.root));
});

//server
gulp.task('connectDev', function () {
  connect.server({
    root: [config.root],
    port: 3000,
    livereload: true
  });
});

//watch
gulp.task('watch', function () {
  gulp.watch(config.CSSSource+'**/*', ['sass-sourcemaps']);
  gulp.watch(config.jsSource+'/**/*.js', ['js']);
  gulp.watch(config.htmlSourse + '*.html', ['fileinclude']);
  gulp.watch(config.htmlSourse + 'partials/*.html', ['fileinclude']);
});


/*
 * global tasks
 *
 */
gulp.task('default', ['watch', 'js', 'sass-sourcemaps', 'connectDev', 'fileinclude']);
gulp.task('prod', ['sass-no-sourcemaps', 'js']);

gulp.task('generateHtml', ['fileinclude']);
